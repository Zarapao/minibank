﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using miniBank.DAL.Enums;
using miniBank.DAL.Interfaces;
using miniBank.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace miniBank.DAL.UnitOfWork
{
    public class UnitOfWork<TContext> : IUnitOfWork<TContext> where TContext : DbContext
    {
        private Dictionary<Type, object> _repositories;
        public ContextFactory ContxFactory { get; set; } = ContextFactory.miniBankContext;
        public TContext Context { get; set; }
        public UnitOfWork(TContext context)
        {
            Context = context ?? throw new ArgumentNullException(nameof(context));
        }
        public int SaveChanges()
        {
            return Context.SaveChanges();
        }
        public void Dispose()
        {
            Context?.Dispose();
        }
        public IRepository<TEntity> GetRepository<TEntity>() where TEntity : class
        {
            if (_repositories == null) _repositories = new Dictionary<Type, object>();

            var type = typeof(TEntity);
            if (!_repositories.ContainsKey(type)) _repositories[type] = new Repository<TEntity>(Context);

            return (IRepository<TEntity>)_repositories[type];
        }
        public IDbContextTransaction BeginTransaction()
        {
            return this.Context.Database.BeginTransaction();
        }
    }
}
