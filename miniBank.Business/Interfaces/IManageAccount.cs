﻿using miniBank.Business.Commons;
using miniBank.Business.Models.ManageAccount;
using System;
using System.Collections.Generic;
using System.Text;

namespace miniBank.Business.Interfaces
{
    public interface IManageAccount
    {
        ActionResult CreateAccount(CreateAccountParameter param);
        DataResult ListAccount();
        ActionResult UpdateFee(UpdateFeeParameter param);
        DataResult FeeResult();
    }
}
