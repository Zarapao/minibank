﻿using miniBank.Business.Commons;
using miniBank.Business.Models.CustomerAction;
using System;
using System.Collections.Generic;
using System.Text;

namespace miniBank.Business.Interfaces
{
    public interface ICustomerAction
    {
        ActionResult Deposit(ActionParameter param);
        ActionResult Withdraw(ActionParameter param);
        ActionResult Tranfer(ActionParameter param);
        DataResult History(HistoryParameter param);
        DataResult ListAccount(ListAccountParameter param);
        ActionResult Login(LoginParameter param);
    }
}
