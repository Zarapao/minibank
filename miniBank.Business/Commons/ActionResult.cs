﻿using System;
using System.Collections.Generic;
using System.Text;

namespace miniBank.Business.Commons
{
    public class ActionResult
    {
        public bool Status { get; set; }
        public string Message { get; set; }
        public string DetailAction { get; set; }
        public dynamic ActionId { get; set; }
        public dynamic Data { get; set; }
    }
}
