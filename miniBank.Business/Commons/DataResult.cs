﻿using System;
using System.Collections.Generic;
using System.Text;

namespace miniBank.Business.Commons
{
    public class DataResult
    {
        public bool Status { get; set; }
        public string Message { get; set; }
        public dynamic Data { get; set; }
    }
}
