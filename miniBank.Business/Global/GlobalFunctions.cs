﻿using System;
using System.Collections.Generic;
using System.Text;

namespace miniBank.Business.Global
{
    public static class GlobalFunctions
    {
        public static double calculateFee(double amount,double fee)
        {
            return Math.Ceiling(((amount * fee) / 100)*100)/100;
        }

        public static double double2digit(double value)
        {
            return Math.Ceiling(value*100)/100;
        }
    }
}
