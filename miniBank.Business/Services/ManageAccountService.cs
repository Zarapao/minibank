﻿using miniBank.Business.Commons;
using miniBank.Business.Interfaces;
using miniBank.Business.Models.ManageAccount;
using miniBank.DAL.Domain.Entities;
using miniBank.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace miniBank.Business.Services
{
    public class ManageAccountService : IManageAccount
    {
        private readonly IUnitOfWork _unitOfWork;
        public ManageAccountService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public ActionResult CreateAccount(CreateAccountParameter param)
        {
            ActionResult result = new ActionResult();

            var repoCustomer = _unitOfWork.GetRepository<Customer>();
            var repoBankAccount = _unitOfWork.GetRepository<BankAccount>();

            using (var transaction = _unitOfWork.BeginTransaction())
            {
                try
                {
                    var dataBankAccount = repoBankAccount.Get(q => q.IBANNumber == param.IBANNumber).FirstOrDefault();

                    if (dataBankAccount != null)
                    {
                        result.Status = false;
                        result.Message = "Create account fail : IBAN number is already.";
                        return result;
                    }

                    var dataCustomer = repoCustomer.Get(q => q.idCard == param.IDCard).FirstOrDefault();

                    Customer dataAdd = new Customer();
                    var idCard = param.IDCard;
                    Guid customerId;
                    string passwordGenerate = "1234";

                    if (dataCustomer != null)
                    {
                        idCard = dataCustomer.idCard;
                        passwordGenerate = dataCustomer.password;
                        customerId = dataCustomer.customerId;
                    }
                    else
                    {
                        dataAdd.idCard = idCard;
                        dataAdd.password = passwordGenerate;
                        repoCustomer.Add(dataAdd);
                        _unitOfWork.SaveChanges();
                        customerId = dataAdd.customerId;
                    }

                    repoBankAccount.Add(new BankAccount
                    {
                        customerId = customerId,
                        IBANNumber = param.IBANNumber,
                        pinCode = param.pinCode
                    });
                    _unitOfWork.SaveChanges();


                    transaction.Commit();
                    result.Status = true;
                    result.Message = "Create account success.";
                    result.ActionId = new
                    {
                        UserName = idCard,
                        Password = passwordGenerate
                    };

                    return result;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    result.Message = "Create account fail : " + ex.Message;

                    return result;
                }
            }
        }

        public DataResult ListAccount()
        {
            DataResult result = new DataResult();
            List<AccountDetail> data = new List<AccountDetail>();

            try
            {
                var repoBankAccount = _unitOfWork.GetRepository<BankAccount>().Include(i => i.customer).Where(q => q.isDeleted == false).ToList();

                foreach (var run in repoBankAccount)
                {
                    data.Add(new AccountDetail
                    {
                        idCard = run.customer.idCard,
                        pincode = run.pinCode,
                        IBANNumber = run.IBANNumber
                    });
                }

                result.Status = true;
                result.Message = "Get data success.";
                result.Data = data;

                return result;
            }
            catch (Exception ex)
            {
                result.Status = false;
                result.Message = "Get data fail : " + ex.Message;

                return result;
            }
        }

        public ActionResult UpdateFee(UpdateFeeParameter param)
        {
            ActionResult result = new ActionResult();

            var repoTypeTransaction = _unitOfWork.GetRepository<TypeTransaction>();

            using (var transaction = _unitOfWork.BeginTransaction())
            {
                try
                {
                    var dataTypeTransaction = repoTypeTransaction.Get(q => q.isDeleted == false && q.typeTransactionId != 4).ToList();

                    foreach (var run in dataTypeTransaction)
                    {
                        if (run.typeTransactionId == 1)
                        {
                            run.fee = param.depositFee;
                        }
                        if (run.typeTransactionId == 2)
                        {
                            run.fee = param.withdrawFee;
                        }
                        if (run.typeTransactionId == 3)
                        {
                            run.fee = param.tranferFee;
                        }

                        repoTypeTransaction.Update(run);
                        _unitOfWork.SaveChanges();
                    }

                    transaction.Commit();

                    result.Status = true;
                    result.Message = "Update success.";

                    return result;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();

                    result.Status = false;
                    result.Message = "Update fail : " + ex.Message;

                    return result;
                }
            }


        }

        public DataResult FeeResult()
        {
            DataResult result = new DataResult();
            List<FeeResponse> data = new List<FeeResponse>();

            var repoTypeTransaction = _unitOfWork.GetRepository<TypeTransaction>().Get(q => q.isDeleted == false && q.typeTransactionId != 4).ToList();

            try { 

            foreach (var run in repoTypeTransaction)
            {
                data.Add(new FeeResponse
                {
                    typeTransactionId = run.typeTransactionId,
                    typeTransactionName = run.typeTransactionName,
                    fee = run.fee
                });
            }
                result.Status = true;
                result.Message = "Get data success.";
                result.Data = data;
                return result;
            }
            catch(Exception ex)
            {
                result.Status = false;
                result.Message = "Get data fail : " + ex.Message;
                return result;
            }

            
        }
    }
}
