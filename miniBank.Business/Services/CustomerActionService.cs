﻿using miniBank.Business.Commons;
using miniBank.Business.Global;
using miniBank.Business.Interfaces;
using miniBank.Business.Models.CustomerAction;
using miniBank.DAL.Domain.Entities;
using miniBank.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace miniBank.Business.Services
{
    public class CustomerActionService : ICustomerAction
    {
        private readonly IUnitOfWork _unitOfWork;
        private CultureInfo en = new CultureInfo("en-EN");
        public CustomerActionService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public ActionResult Deposit(ActionParameter param)
        {
            ActionResult result = new ActionResult();

            var repoTransaction = _unitOfWork.GetRepository<Transaction>();
            var repoBankAccount = _unitOfWork.GetRepository<BankAccount>();

            using (var transaction = _unitOfWork.BeginTransaction())
            {
                try
                {
                    var repoTypeTransaction = _unitOfWork.GetRepository<TypeTransaction>().Get(q => q.typeTransactionId == 1).FirstOrDefault();
                    var dataBankAccount = repoBankAccount.Get(q => q.isDeleted == false && q.IBANNumber == param.toIBANNumber && q.pinCode == param.pinCode).FirstOrDefault();

                    if (dataBankAccount == null)
                    {
                        result.Status = false;
                        result.Message = "Deposit fail : Account or pincode invalid.";
                        return result;
                    }

                    double feeAmount = GlobalFunctions.calculateFee(param.amount, repoTypeTransaction.fee);

                    Transaction dataAddTransaction = new Transaction();
                    dataAddTransaction.DateAction = DateTime.Now;
                    dataAddTransaction.typeTransactionId = 1;
                    dataAddTransaction.toAccountId = dataBankAccount.IBANNumber;
                    dataAddTransaction.amount = param.amount;
                    //dataAddTransaction.feePercent = repoTypeTransaction.fee;
                    //dataAddTransaction.feeAmount = feeAmount;
                    dataAddTransaction.totalAmount = param.amount;
                    dataAddTransaction.balanceAmountTo = GlobalFunctions.double2digit(dataBankAccount.Balance + param.amount);

                    repoTransaction.Add(dataAddTransaction);
                    _unitOfWork.SaveChanges();

                    if (feeAmount > 0)
                    {
                        repoTransaction.Add(new Transaction
                        {
                            DateAction = DateTime.Now,
                            referenceOfFeeId = dataAddTransaction.transactionId,
                            typeTransactionId = 4,
                            fromAccountId = dataBankAccount.IBANNumber,
                            amount = param.amount,
                            feePercent = repoTypeTransaction.fee,
                            feeAmount = feeAmount,
                            totalAmount = feeAmount,
                            balanceAmountFrom = GlobalFunctions.double2digit(dataBankAccount.Balance + (param.amount - feeAmount))
                        });
                        _unitOfWork.SaveChanges();
                    }

                    dataBankAccount.Balance = GlobalFunctions.double2digit(dataBankAccount.Balance + (param.amount - feeAmount));
                    repoBankAccount.Update(dataBankAccount);
                    _unitOfWork.SaveChanges();

                    transaction.Commit();

                    result.Status = true;
                    result.Message = "Deposit success.";

                    return result;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();

                    result.Status = false;
                    result.Message = "Deposit fail : " + ex.Message;

                    return result;
                }
            }
        }

        public ActionResult Withdraw(ActionParameter param)
        {
            ActionResult result = new ActionResult();

            var repoTransaction = _unitOfWork.GetRepository<Transaction>();
            var repoBankAccount = _unitOfWork.GetRepository<BankAccount>();

            using (var transaction = _unitOfWork.BeginTransaction())
            {
                try
                {
                    var repoTypeTransaction = _unitOfWork.GetRepository<TypeTransaction>().Get(q => q.typeTransactionId == 2).FirstOrDefault();
                    var dataBankAccount = repoBankAccount.Get(q => q.isDeleted == false && q.IBANNumber == param.fromIBANNumber && q.pinCode == param.pinCode).FirstOrDefault();

                    if (dataBankAccount == null)
                    {
                        result.Status = false;
                        result.Message = "Withdraw fail : Account or pincode invalid.";
                        return result;
                    }

                    double feeAmount = GlobalFunctions.calculateFee(param.amount, repoTypeTransaction.fee);
                    double balance = GlobalFunctions.double2digit(dataBankAccount.Balance - (param.amount + feeAmount));
                    if (balance < 0)
                    {
                        result.Status = false;
                        result.Message = "Withdraw fail : not enough money;";
                        return result;
                    }

                    Transaction dataAddTransaction = new Transaction();
                    dataAddTransaction.DateAction = DateTime.Now;
                    dataAddTransaction.typeTransactionId = 2;
                    dataAddTransaction.fromAccountId = dataBankAccount.IBANNumber;
                    dataAddTransaction.amount = param.amount;
                    //dataAddTransaction.feePercent = repoTypeTransaction.fee;
                    //dataAddTransaction.feeAmount = feeAmount;
                    dataAddTransaction.totalAmount = param.amount;
                    dataAddTransaction.balanceAmountFrom = GlobalFunctions.double2digit(dataBankAccount.Balance - param.amount);

                    repoTransaction.Add(dataAddTransaction);
                    _unitOfWork.SaveChanges();

                    if (feeAmount > 0)
                    {
                        repoTransaction.Add(new Transaction
                        {
                            DateAction = DateTime.Now,
                            referenceOfFeeId = dataAddTransaction.transactionId,
                            typeTransactionId = 4,
                            fromAccountId = dataBankAccount.IBANNumber,
                            amount = param.amount,
                            feePercent = repoTypeTransaction.fee,
                            feeAmount = feeAmount,
                            totalAmount = feeAmount,
                            balanceAmountFrom = balance
                        });
                        _unitOfWork.SaveChanges();
                    }

                    dataBankAccount.Balance = balance;
                    repoBankAccount.Update(dataBankAccount);
                    _unitOfWork.SaveChanges();

                    transaction.Commit();

                    result.Status = true;
                    result.Message = "Withdraw success.";

                    return result;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();

                    result.Status = false;
                    result.Message = "Withdraw fail : " + ex.Message;

                    return result;
                }

            }
        }

        public ActionResult Tranfer(ActionParameter param)
        {
            ActionResult result = new ActionResult();

            var repoTransaction = _unitOfWork.GetRepository<Transaction>();
            var repoBankAccount = _unitOfWork.GetRepository<BankAccount>();

            using (var transaction = _unitOfWork.BeginTransaction())
            {
                try
                {
                    if (param.fromIBANNumber == param.toIBANNumber)
                    {
                        result.Status = false;
                        result.Message = "Tranfer fail : Can not tranfer same Account";
                        return result;
                    }

                    var dataTranferOut = _unitOfWork.GetRepository<TypeTransaction>().Get(q => q.typeTransactionId == 3).FirstOrDefault();

                    var dataBankAccountOut = repoBankAccount.Get(q => q.isDeleted == false && q.IBANNumber == param.fromIBANNumber && q.pinCode == param.pinCode).FirstOrDefault();
                    var dataBankAccountIn = repoBankAccount.Get(q => q.isDeleted == false && q.IBANNumber == param.toIBANNumber).FirstOrDefault();

                    if (dataBankAccountOut == null)
                    {
                        result.Status = false;
                        result.Message = "Tranfer fail : Account or pincode invalid.";
                        return result;
                    }

                    double feeAmount = GlobalFunctions.calculateFee(param.amount, dataTranferOut.fee);
                    double balanceFrom = GlobalFunctions.double2digit(dataBankAccountOut.Balance - (param.amount + feeAmount));
                    double balancerTo = GlobalFunctions.double2digit(dataBankAccountIn.Balance + param.amount);
                    if (balanceFrom < 0)
                    {
                        result.Status = false;
                        result.Message = "Tranfer fail : not enough money";
                        return result;
                    }

                    Transaction dataAddTransaction = new Transaction();
                    dataAddTransaction.DateAction = DateTime.Now;
                    dataAddTransaction.typeTransactionId = 3;
                    dataAddTransaction.fromAccountId = dataBankAccountOut.IBANNumber;
                    dataAddTransaction.toAccountId = param.toIBANNumber;
                    dataAddTransaction.amount = param.amount;
                    //dataAddTransaction.feePercent = dataTranferOut.fee;
                    //dataAddTransaction.feeAmount = feeAmount;
                    dataAddTransaction.totalAmount = param.amount;
                    dataAddTransaction.balanceAmountFrom = GlobalFunctions.double2digit(dataBankAccountOut.Balance - param.amount);
                    dataAddTransaction.balanceAmountTo = balancerTo;

                    repoTransaction.Add(dataAddTransaction);
                    repoTransaction.Add(dataAddTransaction);
                    _unitOfWork.SaveChanges();

                    if (feeAmount > 0)
                    {
                        repoTransaction.Add(new Transaction
                        {
                            DateAction = DateTime.Now,
                            referenceOfFeeId = dataAddTransaction.transactionId,
                            typeTransactionId = 4,
                            fromAccountId = dataBankAccountOut.IBANNumber,
                            amount = param.amount,
                            feePercent = dataTranferOut.fee,
                            feeAmount = feeAmount,
                            totalAmount = feeAmount,
                            balanceAmountFrom = balanceFrom
                        });
                        _unitOfWork.SaveChanges();
                    }



                    dataBankAccountOut.Balance = balanceFrom;
                    repoBankAccount.Update(dataBankAccountOut);
                    _unitOfWork.SaveChanges();

                    dataBankAccountIn.Balance = balancerTo;
                    repoBankAccount.Update(dataBankAccountIn);
                    _unitOfWork.SaveChanges();


                    transaction.Commit();

                    result.Status = true;
                    result.Message = "Tranfer success.";

                    return result;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    result.Status = false;
                    result.Message = "Tranfer fail : " + ex.Message;

                    return result;
                }

            }
        }

        public DataResult History(HistoryParameter param)
        {
            DataResult result = new DataResult();
            List<HistoryDetail> data = new List<HistoryDetail>();
            try
            {

                var repoTransaction = _unitOfWork.GetRepository<Transaction>();
                var dataTransaction = repoTransaction.Include(i => i.typeTransaction, j => j.referenceOfFee).Where(q => q.isDeleted == false
                                             && (q.fromAccountId == param.IBANNumber || q.toAccountId == param.IBANNumber)).ToList().OrderByDescending(q => q.DateAction);
                var dataTransactionRef = repoTransaction.Include(i => i.typeTransaction).Where(q => q.isDeleted == false
                                          && (q.fromAccountId == param.IBANNumber || q.toAccountId == param.IBANNumber) && q.referenceOfFeeId != null);

                foreach (var run in dataTransaction)
                {
                    string balance = "";
                    string amount = "";
                    if (run.typeTransactionId == 1)
                    {
                        amount = "+" + run.totalAmount.ToString();
                        balance = run.balanceAmountTo.ToString();
                    }
                    else if (run.typeTransactionId == 2)
                    {
                        amount = "-" + run.totalAmount.ToString();
                        balance = run.balanceAmountFrom.ToString();
                    }
                    else if (run.typeTransactionId == 3)
                    {
                        if (run.toAccountId == param.IBANNumber)
                        {
                            amount = "+" + run.totalAmount.ToString();
                            balance = run.balanceAmountTo.ToString();
                        }
                        else if (run.fromAccountId == param.IBANNumber)
                        {
                            amount = "-" + run.totalAmount.ToString();
                            balance = run.balanceAmountFrom.ToString();
                        }
                    }
                    else if (run.typeTransactionId == 4)
                    {
                        amount = "-" + run.totalAmount.ToString();
                        balance = run.balanceAmountFrom.ToString();
                    }
                    else
                    {
                        amount = run.totalAmount.ToString();
                        balance = run.balanceAmountFrom.ToString();
                    }
                    data.Add(new HistoryDetail
                    {
                        transactionId = run.transactionId.ToString(),
                        typeTransactionId = run.typeTransactionId,
                        typeTransactionName = run.typeTransaction.typeTransactionName + (run.typeTransactionId == 4 ? " of " + run.referenceOfFee.typeTransaction.typeTransactionName : ""),
                        dateAction = run.DateAction.ToString("d/MMM/yyyy HH:mm:ss", en),
                        amount = amount,
                        balance = balance,
                    });
                }

                result.Status = true;
                result.Message = "Get history success.";
                result.Data = data;

                return result;
            }
            catch (Exception ex)
            {
                result.Status = false;
                result.Message = "Get history fail : " + ex.Message;
                return result;
            }
        }

        public DataResult ListAccount(ListAccountParameter param)
        {
            DataResult result = new DataResult();
            List<AccountDetail> data = new List<AccountDetail>();
 
            try
            {
                var dataBankAccount = _unitOfWork.GetRepository<BankAccount>().Get(q => q.isDeleted == false && q.customerId.ToString() == param.customerId).ToList();

                var repoTransaction = _unitOfWork.GetRepository<Transaction>();
                var getTransaction = repoTransaction.Include(i => i.typeTransaction, j => j.referenceOfFee).Where(q => q.isDeleted == false).ToList();
                var getTransactionRef = repoTransaction.Include(i => i.typeTransaction).Where(q => q.isDeleted == false).ToList();

                foreach (var run in dataBankAccount)
                {
                    List<HistoryDetail> listDetail = new List<HistoryDetail>();
                    var dataTransaction = getTransaction.Where(q => (q.fromAccountId == run.IBANNumber || q.toAccountId == run.IBANNumber)).ToList().OrderByDescending(q => q.DateAction);
                    var dataTransactionRef = getTransactionRef.Where(q => (q.fromAccountId == run.IBANNumber || q.toAccountId == run.IBANNumber) && q.referenceOfFeeId != null);

                    foreach (var runTran in dataTransaction)
                    {
                        string balance = "";
                        string amount = "";
                        string typeAmount = "black";
                        if (runTran.typeTransactionId == 1)
                        {
                            amount = "+" + runTran.totalAmount.ToString();
                            balance = runTran.balanceAmountTo.ToString();
                            typeAmount = "green";
                        }
                        else if (runTran.typeTransactionId == 2)
                        {
                            amount = "-" + runTran.totalAmount.ToString();
                            balance = runTran.balanceAmountFrom.ToString();
                            typeAmount = "red";
                        }
                        else if (runTran.typeTransactionId == 3)
                        {
                            if (runTran.toAccountId == run.IBANNumber)
                            {
                                amount = "+" + runTran.totalAmount.ToString();
                                balance = runTran.balanceAmountTo.ToString();
                                typeAmount = "green";
                            }
                            else if (runTran.fromAccountId == run.IBANNumber)
                            {
                                amount = "-" + runTran.totalAmount.ToString();
                                balance = runTran.balanceAmountFrom.ToString();
                                typeAmount = "red";
                            }
                        }
                        else if (runTran.typeTransactionId == 4)
                        {
                            amount = "-" + runTran.totalAmount.ToString();
                            balance = runTran.balanceAmountFrom.ToString();
                            typeAmount = "red";
                        }
                        else
                        {
                            amount = runTran.totalAmount.ToString();
                            balance = runTran.balanceAmountFrom.ToString();
                            typeAmount = "black";
                        }
                        listDetail.Add(new HistoryDetail
                        {
                            transactionId = runTran.transactionId.ToString(),
                            typeTransactionId = runTran.typeTransactionId,
                            typeTransactionName = runTran.typeTransaction.typeTransactionName + (runTran.typeTransactionId == 4 ? " of " + runTran.referenceOfFee.typeTransaction.typeTransactionName : ""),
                            dateAction = runTran.DateAction.ToString("d/MMM/yyyy HH:mm:ss", en),
                            amount = amount,
                            balance = balance,
                            colorAmount = typeAmount
                        });
                    }

                    data.Add(new AccountDetail
                    {
                        bankAccountId = run.bankAccountId.ToString(),
                        accoutName = run.AccountName,
                        IBANNumber = run.IBANNumber,
                        balance = run.Balance.ToString(),
                        listHistory = listDetail
                    });
                }

                result.Status = true;
                result.Message = "Get data success.";
                result.Data = data;
                return result;
            }
            catch (Exception ex)
            {
                result.Status = false;
                result.Message = "Get data fail : " + ex.Message;
                return result;
            }
        }

        public ActionResult Login(LoginParameter param)
        {
            ActionResult result = new ActionResult();

            var dataCustomer = _unitOfWork.GetRepository<Customer>().Get(q => q.idCard == param.idCard && q.password == param.password && q.isDeleted == false).FirstOrDefault();

            if (dataCustomer != null)
            {
                result.Status = true;
                result.Message = "Login Success.";
                result.Data = new
                {
                    customerId = dataCustomer.customerId
                };

                return result;
            }
            else
            {
                result.Status = false;
                result.Message = "Login fail.";

                return result;
            }


        }
    }
}
