﻿using System;
using System.Collections.Generic;
using System.Text;

namespace miniBank.Business.Models.CustomerAction
{
    public class HistoryDetail
    {
        public string transactionId { get; set; }
        public int typeTransactionId { get; set; }
        public string typeTransactionName { get; set; }
        public string dateAction { get; set; }
        public string amount { get; set; }
        public string balance { get; set; }
        public string colorAmount { get; set; } 

    }
}
