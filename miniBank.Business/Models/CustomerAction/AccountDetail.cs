﻿using System;
using System.Collections.Generic;
using System.Text;

namespace miniBank.Business.Models.CustomerAction
{
    public class AccountDetail
    {
        public string bankAccountId { get; set; }
        public string accoutName { get; set; }
        public string IBANNumber { get; set; }
        public string balance { get; set; }
        public List<HistoryDetail> listHistory { get; set; }
    }
}
