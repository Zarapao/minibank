﻿using System;
using System.Collections.Generic;
using System.Text;

namespace miniBank.Business.Models.CustomerAction
{
    public class ActionParameter
    {
        public string fromIBANNumber { get; set; }
        public string toIBANNumber { get; set; }
        public double amount { get; set; }
        public string pinCode { get; set; }
    }
}
