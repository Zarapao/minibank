﻿using System;
using System.Collections.Generic;
using System.Text;

namespace miniBank.Business.Models.CustomerAction
{
    public class LoginParameter
    {
        public string idCard { get; set; }
        public string password { get; set; }
    }
}
