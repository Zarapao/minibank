﻿using System;
using System.Collections.Generic;
using System.Text;

namespace miniBank.Business.Models.ManageAccount
{
    public class AccountDetail
    {
        public string idCard { get; set; }
        public string IBANNumber { get; set; }
        public string pincode { get; set; }
    }
}
