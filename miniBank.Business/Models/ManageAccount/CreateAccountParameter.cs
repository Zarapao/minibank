﻿using System;
using System.Collections.Generic;
using System.Text;

namespace miniBank.Business.Models.ManageAccount
{
    public class CreateAccountParameter
    {
        public string IBANNumber { get; set; }
        public string IDCard { get; set; }
        public string customerName { get; set; }
        public string pinCode { get; set; }
    }
}
