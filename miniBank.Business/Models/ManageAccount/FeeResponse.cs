﻿using System;
using System.Collections.Generic;
using System.Text;

namespace miniBank.Business.Models.ManageAccount
{
    public class FeeResponse
    {
        public int typeTransactionId { get; set; }
        public string typeTransactionName { get; set; }
        public double fee { get; set; }
    }
}
