﻿using System;
using System.Collections.Generic;
using System.Text;

namespace miniBank.Business.Models.ManageAccount
{
    public class UpdateFeeParameter
    {
        public double depositFee { get; set; }
        public double withdrawFee { get; set; }
        public double tranferFee { get; set; }
    }
}
