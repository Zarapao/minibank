﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace miniBank.DAL.Domain.Entities
{
    [Table("Transaction")]
    public class Transaction : _BaseEntity
    {
        [Key]
        [Column("TransactionId")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid transactionId { get; set; }

        [Column("DateAction")]
        public DateTime DateAction { get; set; }

        [Column("ReferenceOfFeeId")]
        public Guid? referenceOfFeeId { get; set; }

        [ForeignKey("referenceOfFeeId")]
        public virtual Transaction referenceOfFee { get; set; }

        [Column("TypeTransactionId")]
        [Required]
        public int typeTransactionId { get; set; }

        [ForeignKey("typeTransactionId")]
        public virtual TypeTransaction typeTransaction { get; set; }

        [Column("FromAccountId")]
        public string fromAccountId { get; set; }

        [Column("ToAccountId")]
        public string toAccountId { get; set; }

        [Column("Amount")]
        public double amount { get; set; }

        [Column("FeePercent")]
        public double feePercent { get; set; }

        [Column("FeeAmount")]
        public double feeAmount { get; set; }

        [Column("TotalAmount")]
        public double totalAmount { get; set; }

        [Column("BalanceAmountFrom")]
        public double balanceAmountFrom { get; set; }

        [Column("BalanceAmountTo")]
        public double balanceAmountTo { get; set; }
        public virtual ICollection<Transaction> transactions { get; set; }
    }

    public class TransactionMap : IEntityTypeConfiguration<Transaction>
    {
        public void Configure(EntityTypeBuilder<Transaction> builder)
        {
            builder.HasOne(s => s.typeTransaction)
                .WithMany(d => d.transactions)
                .HasForeignKey(s => s.typeTransactionId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(s => s.referenceOfFee)
                .WithMany(d => d.transactions)
                .HasForeignKey(s => s.referenceOfFeeId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
