﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace miniBank.DAL.Domain.Entities
{
    [Table("Customer")]
    public class Customer : _BaseEntity
    {
        [Key]
        [Column("CustomerId")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid customerId { get; set; }

        [Column("CustomerName")]
        public string customerName { get; set; }

        [Column("IDCard")]
        public string idCard { get; set; }

        [Column("Password")]
        public string password { get; set; }

        public virtual ICollection<BankAccount> bankAccounts { get; set; }
    }
}
