﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace miniBank.DAL.Domain.Entities
{
    [Table("TypeTransaction")]
    public class TypeTransaction : _BaseEntity
    {
        [Key]
        [Column("TypeTransactionId")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int typeTransactionId { get; set; }

        [Column("TypeTransactionName")]
        public string typeTransactionName { get; set; }

        [Column("Fee")]
        public double fee { get; set; }

        public virtual ICollection<Transaction> transactions { get; set; }
    }
}
