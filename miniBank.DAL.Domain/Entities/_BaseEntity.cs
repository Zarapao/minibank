﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace miniBank.DAL.Domain.Entities
{
    public class _BaseEntity
    {
        public _BaseEntity()
        {
            this.createdDate = DateTime.Now;
            this.isDeleted = false;
        }

        [Column("CreatedDate")]
        public DateTime? createdDate { get; set; }

        [Column("IsDeleted")]
        public bool? isDeleted { get; set; }

        [Column("DeletedDate")]
        public DateTime? deletedDate { get; set; }

    }
}
