﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace miniBank.DAL.Domain.Entities
{
    [Table("BankAccount")]
    public class BankAccount : _BaseEntity
    {
        [Key]
        [Column("BankAccountId")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid bankAccountId { get; set; }

        [Column("CustomerId")]
        [Required]
        public Guid customerId { get; set; }

        [ForeignKey("customerId")]
        public virtual Customer customer { get; set; }

        [Column("AccountName")]
        public string AccountName { get; set; }

        [Column("IBANNumber")]
        public string IBANNumber { get; set; }

        [Column("PinCode")]
        public string pinCode { get; set; }

        [Column("Balance")]
        public double Balance { get; set; } = 0;

    }

    public class BankAccountMap : IEntityTypeConfiguration<BankAccount>
    {
        public void Configure(EntityTypeBuilder<BankAccount> builder)
        {

            builder.HasOne(s => s.customer)
                .WithMany(d => d.bankAccounts)
                .HasForeignKey(s => s.customerId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
