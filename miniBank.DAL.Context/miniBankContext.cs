﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using miniBank.DAL.Domain.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace miniBank.DAL.Context
{
    public class miniBankContext : DbContext
    {
        public DbSet<Customer> Customers { get; set; }
        public DbSet<BankAccount> BankAccounts { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<TypeTransaction> TypeTransactions { get; set; }

        protected override void OnModelCreating(ModelBuilder ModelBuilder)
        {
            Seeders.SeederConfig.SeedConfig(ModelBuilder);
            base.OnModelCreating(ModelBuilder);
        }

        public miniBankContext(DbContextOptions<miniBankContext> options) : base(options)
        {
        }
    }
    public class DesignTDbContextFactory : IDesignTimeDbContextFactory<miniBankContext>
    {
        public miniBankContext CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
                                                                         .AddJsonFile(@Directory.GetCurrentDirectory() + "/../miniBank/appsettings.json")
                                                                         .Build();
            var builder = new DbContextOptionsBuilder<miniBankContext>();
            var connectionString = configuration.GetConnectionString("DBConnection");
            builder.UseMySql(connectionString, ServerVersion.Parse("10.6.4-MariaDB-1:10.6.4+maria~focal"));
            return new miniBankContext(builder.Options);

        }

    }
}
