﻿using Microsoft.EntityFrameworkCore;
using miniBank.DAL.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace miniBank.DAL.Context.Seeders
{
    public class SeederConfig
    {
        public static void SeedConfig(ModelBuilder ModelBuilder)
        {
                ModelBuilder.Entity<TypeTransaction>().HasData(
                new TypeTransaction
                {
                    typeTransactionId = 1,
                    typeTransactionName = "Deposit",
                    fee = 0.1,
                    createdDate = DateTime.Now
                }, new TypeTransaction
                {
                    typeTransactionId = 2,
                    typeTransactionName = "Withdraw",
                    fee = 0,
                    createdDate = DateTime.Now
                }, new TypeTransaction
                {
                    typeTransactionId = 3,
                    typeTransactionName = "Tranfer",
                    fee = 0,
                    createdDate = DateTime.Now
                }, new TypeTransaction
                {
                    typeTransactionId = 4,
                    typeTransactionName = "Fee",
                    fee = 0,
                    createdDate = DateTime.Now
                });
        }
    }
}
