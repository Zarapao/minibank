using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using miniBank.DAL.Context;
using miniBank.DAL.Interfaces;
using miniBank.DAL.UnitOfWork;
using miniBank.ServiceExtension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace miniBank
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddDbContext<miniBankContext>(option => option.UseMySql(Configuration.GetConnectionString("DBConnection"),
                                                    ServerVersion.Parse("10.6.4-MariaDB-1:10.6.4+maria~focal")));

            //Inject (Unit Of Work).
            services.AddScoped<IUnitOfWork, UnitOfWork<miniBankContext>>();

            // Injection T2
            services.RegisterBusiness();

            services.AddCors(o => o.AddPolicy("miniBankPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                       .WithOrigins()
                       .AllowAnyMethod()
                       .WithMethods()
                       .AllowAnyHeader()
                       .WithHeaders()
                       .SetIsOriginAllowedToAllowWildcardSubdomains();
            }));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(options => {
                    options.AllowAnyOrigin();
                    options.AllowAnyHeader();
                    options.AllowAnyMethod();
                });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
