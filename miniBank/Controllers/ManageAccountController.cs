﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using miniBank.Business.Interfaces;
using miniBank.Business.Models.ManageAccount;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace miniBank.Controllers
{
    [Route("/[controller]")]
    [ApiController]
    public class ManageAccountController : ControllerBase
    {
        private readonly IManageAccount _ManageAccountServices;
        public ManageAccountController(IManageAccount ManageAccountServices)
        {
            _ManageAccountServices = ManageAccountServices;
        }

        [HttpPost]
        [Route("CreateAccount")]
        public IActionResult CreateAccount([FromBody]CreateAccountParameter param)
        {
            var result = _ManageAccountServices.CreateAccount(param);

            if (result.Status)
            {
                return Ok(result);
            }
            else
            {
                return BadRequest(result);
            }
        }

        [HttpPost]
        [Route("UpdateFee")]
        public IActionResult UpdateFee([FromBody] UpdateFeeParameter param)
        {
            var result = _ManageAccountServices.UpdateFee(param);

            if (result.Status)
            {
                return Ok(result);
            }
            else
            {
                return BadRequest(result);
            }
        }

        [HttpGet]
        [Route("ListAccount")]
        public IActionResult ListAccount()
        {
            var result = _ManageAccountServices.ListAccount();

            if (result.Status)
            {
                return Ok(result);
            }
            else
            {
                return BadRequest(result);
            }
        }

        [HttpGet]
        [Route("FeeDetail")]
        public IActionResult FeeDetail()
        {
            var result = _ManageAccountServices.FeeResult();

            if (result.Status)
            {
                return Ok(result);
            }
            else
            {
                return BadRequest(result);
            }
        }
    }
}
