﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using miniBank.Business.Interfaces;
using miniBank.Business.Models.CustomerAction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace miniBank.Controllers
{
    [Route("/[controller]")]
    [ApiController]
    public class CustomerActionController : ControllerBase
    {
        private readonly ICustomerAction _CustomerActionService;
        public CustomerActionController(ICustomerAction CustomerActionService)
        {
            _CustomerActionService = CustomerActionService;
        }

        [HttpPost]
        [Route("Deposit")]
        public IActionResult Deposit([FromBody] ActionParameter param)
        {
            var result = _CustomerActionService.Deposit(param);

            if (result.Status)
            {
                return Ok(result);
            }
            else
            {
                return BadRequest(result);
            }
        }

        [HttpPost]
        [Route("Withdraw")]
        public IActionResult WithDraw([FromBody] ActionParameter param)
        {
            var result = _CustomerActionService.Withdraw(param);

            if (result.Status)
            {
                return Ok(result);
            }
            else
            {
                return BadRequest(result);
            }
        }

        [HttpPost]
        [Route("Tranfer")]
        public IActionResult Tranfer([FromBody] ActionParameter param)
        {
            var result = _CustomerActionService.Tranfer(param);

            if (result.Status)
            {
                return Ok(result);
            }
            else
            {
                return BadRequest(result);
            }
        }

        [HttpGet]
        [Route("History")]
        public IActionResult History([FromQuery]HistoryParameter param)
        {
            var result = _CustomerActionService.History(param);

            if (result.Status)
            {
                return Ok(result);
            }
            else
            {
                return BadRequest(result);
            }
        }

        [HttpGet]
        [Route("ListAccount")]
        public IActionResult ListAccount([FromQuery] ListAccountParameter param)
        {
            var result = _CustomerActionService.ListAccount(param);

            if (result.Status)
            {
                return Ok(result);
            }
            else
            {
                return BadRequest(result);
            }
        }

        [HttpPost]
        [Route("Login")]
        public IActionResult Login([FromBody] LoginParameter param)
        {
            var result = _CustomerActionService.Login(param);

            if (result.Status)
            {
                return Ok(result);
            }
            else
            {
                return BadRequest(result);
            }
        }

        [HttpGet]
        [Route("info")]
        public IActionResult info()
        {
            return Ok(new { 
                Name = "BankMini"
            });
        }
    }
}
