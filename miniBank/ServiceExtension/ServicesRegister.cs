﻿using Microsoft.Extensions.DependencyInjection;
using miniBank.Business.Interfaces;
using miniBank.Business.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace miniBank.ServiceExtension
{
    public static class ServicesRegister
    {
        public static IServiceCollection RegisterBusiness(this IServiceCollection services)
        {
            services.AddScoped<IManageAccount, ManageAccountService>();
            services.AddScoped<ICustomerAction, CustomerActionService>();

            return services;
        }
    }
}
